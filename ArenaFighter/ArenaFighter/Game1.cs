using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ArenaFighter {

    // Constants

    public static class Constants {
        public const short GAMEWIDTH = 1280, GAMEHEIGHT = (GAMEWIDTH / 16 * 9);
        public const byte TILE_SIZE = 80;

        // Floor Tiles
        public const byte FLOOR_STD = 0, FLOOR_TWO = 1;

    }


    public class Game1 : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;


        // Texture Dictionary
        public static Dictionary<Byte, Texture2D> TileTextures = new Dictionary<Byte, Texture2D>();

        // World
        Level level;


        public static int times = 0;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = Constants.GAMEWIDTH;
            graphics.PreferredBackBufferHeight = Constants.GAMEHEIGHT;

            this.IsMouseVisible = true;
        }

        protected override void Initialize() {

            level = new Level();
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            PopulateDictionary();   // Call here to ensure GraphicsDevice exists prior to calling

            level.LoadContent(Content);
        }

        public void PopulateDictionary() {
            SpriteSheet MapSheet = new SpriteSheet(GraphicsDevice, Content.Load<Texture2D>("images/spritesheets/MapTileSheet"));

            // MapSheet.GetTile X and Y are the 0-based tile number on X and Y in the sheet - First tile is 0,0 second is 1,0, etc
            TileTextures.Add(Constants.FLOOR_STD, MapSheet.GetTile(0, 0, Constants.TILE_SIZE, Constants.TILE_SIZE));
            TileTextures.Add(Constants.FLOOR_TWO, MapSheet.GetTile(1, 0, Constants.TILE_SIZE, Constants.TILE_SIZE));
        }

        protected override void UnloadContent() { }


        KeyboardState kbstate, oldkbstate;
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed) this.Exit();

            kbstate = Keyboard.GetState(PlayerIndex.One);

            if (kbstate.IsKeyDown(Keys.Space) && !oldkbstate.IsKeyDown(Keys.Space)) {
                level.ChangeTiles();
            }

            level.Update(gameTime);

            oldkbstate = kbstate;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            level.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}

