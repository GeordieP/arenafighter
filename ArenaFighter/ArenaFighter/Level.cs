﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ArenaFighter {
    public class Level {

        short MAP_IMG_WIDTH, MAP_IMG_HEIGHT;        // width and height of the map layout image
        private Texture2D mapLayoutImage;
        public Tile[,] tileMap;         // 2 dimensional array for tiles
        public Color[] pixelColor1d;     // 1 dimensional pixel colors
        public Color[,] pixelColor2d;    // 2 dimensional pixel colors

        public Level() { }


        public void LoadContent(ContentManager Content) {
            mapLayoutImage = Content.Load<Texture2D>("images/MapLayoutImage");

            MAP_IMG_WIDTH = (short)mapLayoutImage.Width;
            MAP_IMG_HEIGHT = (short)mapLayoutImage.Height;

            tileMap = new Tile[MAP_IMG_WIDTH, MAP_IMG_HEIGHT];

            pixelColor1d = new Color[MAP_IMG_WIDTH * MAP_IMG_HEIGHT];
            pixelColor2d = new Color[MAP_IMG_WIDTH, MAP_IMG_HEIGHT];

            GetPixels();
            CreateMap();
        }

        public void GetPixels() {
            mapLayoutImage.GetData<Color>(pixelColor1d);

            int x = 0, y = 0;
            for (int i = 0; i < pixelColor1d.Length; i++) {
                pixelColor2d[x, y] = pixelColor1d[i];
                x++;
                if (x == pixelColor2d.GetLength(0)) {
                    x = 0;
                    y++;
                }
            }
        }

        public void CreateMap() {
            for (int i = 0; i < tileMap.GetLength(0); i++) {
                for (int j = 0; j < tileMap.GetLength(1); j++) {
                    tileMap[i, j] = new Tile(pixelColor2d[i, j], i, j);
                }
            }
            Console.WriteLine("Loading Map...");
        }

        // temp variables
        float counter;
        int index = 0;
        const int EFFECT_TIME_STEP = 100;
        const int TILE_RESET_TIMEOUT = 1000;
        bool changingTiles = false, tileResetTimeout = false;

        public void Update(GameTime gameTime) {
            if (changingTiles) {
                counter += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (counter >= EFFECT_TIME_STEP) {
                    counter = 0;

                    tileMap[markedTiles[index].X, markedTiles[index].Y].texture = Game1.TileTextures[Constants.FLOOR_TWO];
                    tileMap[markedTiles[index].X, markedTiles[index].Y].TileType = Constants.FLOOR_TWO;
                    index++;
                }

                if (index == markedTiles.Count) {
                    changingTiles = false;
                    markedTiles.Clear();
                    counter = 0;
                    index = 0;
                    tileResetTimeout = true;
                }
            }
            else if (tileResetTimeout) {
                counter += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (counter >= TILE_RESET_TIMEOUT) {
                    foreach (Tile t in tileMap) {
                        if (t.TileType == Constants.FLOOR_TWO) {
                            t.texture = Game1.TileTextures[Constants.FLOOR_STD];
                            t.TileType = Constants.FLOOR_STD;
                        }
                    }

                    counter = 0;
                    tileResetTimeout = false;
                }
            }
        }

        private List<Point> markedTiles = new List<Point>();        // Use point type because it can store coordinates

        int times = 0;
        public void ChangeTiles() {
            for (int i = 0; i < tileMap.GetLength(0); i++) {
                for (int j = 0; j < tileMap.GetLength(1); j++) {
                    if (times == 0) {
                        if ((j - i) % 4 == 0)
                            markedTiles.Add(new Point(i, j));
                    }
                    else if (times == 1) {
                        if ((j * i) % 8 == 0)
                            markedTiles.Add(new Point(i, j));
                    }
                }
            }


            //markedTiles.Add(new Point(9 - 1, 4));
            //markedTiles.Add(new Point(9, 4 - 1));

            //markedTiles.Add(new Point(9 + 1, 4));
            //markedTiles.Add(new Point(9, 4 + 1));

            ////

            //markedTiles.Add(new Point(9 - 2, 4 - 2));
            //markedTiles.Add(new Point(9 - 2, 4 - 2));

            //markedTiles.Add(new Point(9 + 2, 4 + 2));
            //markedTiles.Add(new Point(9 + 2, 4 + 2));

            //markedTiles.Add(new Point(9 + 2, 4 - 2));
            //markedTiles.Add(new Point(9 + 2, 4 - 2));

            //markedTiles.Add(new Point(9 - 2, 4 + 2));
            //markedTiles.Add(new Point(9 - 2, 4 + 2));

            if (times < 1) times++;

            changingTiles = true;
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (Tile t in tileMap) {
                spriteBatch.Draw(t.texture, t.destination, Color.White);
            }
        }
    }
}
