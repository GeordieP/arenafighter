﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ArenaFighter {
    public class SpriteSheet {
        private Texture2D Texture;
        GraphicsDevice graphicsDevice;

        public SpriteSheet(GraphicsDevice graphicsDevice, Texture2D texture) {
            this.graphicsDevice = graphicsDevice;
            this.Texture = texture;
        }

        public Texture2D GetTile(int x, int y, int w, int h) {          // xywh is the rectangle to retreive tile from the spritesheet
            RenderTarget2D newTexture = new RenderTarget2D(graphicsDevice, w, h);
            SpriteBatch spriteBatch = new SpriteBatch(graphicsDevice);

            // Render to the texture
            graphicsDevice.SetRenderTarget(newTexture);

            graphicsDevice.Clear(Color.White);

            // draw image to new texture
            spriteBatch.Begin();
            spriteBatch.Draw(Texture, Vector2.Zero, new Rectangle(x * Constants.TILE_SIZE, y * Constants.TILE_SIZE, w, h), Color.White);
            spriteBatch.End();

            // set render target back to screen
            graphicsDevice.SetRenderTarget(null);

            return newTexture;      // if any problems arise, try casting this as a Texture2D
        }
    }
}
