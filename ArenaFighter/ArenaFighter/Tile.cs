﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ArenaFighter {
    public class Tile {
        private const byte TILE_SIZE = Constants.TILE_SIZE;
        public byte TileType;
        public Texture2D texture;
        public Vector2 destination;

        public Color StandardFloorColor = new Color(22, 34, 78, 255);
        public Color FloorTwoColor = Color.White;

        public Tile(Color pixelColor, int i, int j) {

            // Set the type of tile based on color of representitive pixel
            if (pixelColor == StandardFloorColor) {
                TileType = Constants.FLOOR_STD;
            }
            else if (pixelColor == FloorTwoColor) {
                TileType = Constants.FLOOR_TWO;
            }

            texture = Game1.TileTextures[TileType];
            destination = new Vector2(i * TILE_SIZE, j * TILE_SIZE);
        }
    }
}
